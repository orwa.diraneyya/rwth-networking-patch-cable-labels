# Generate Label Text Files for Networking Patch Cables

```
  Hello Orwa,
  the clips always come as a pair.
  · a white clip for network jack number
  · a yellow clip for the switch port number

                yellow    white        yellow    white     
  [Gi2/0/01]<-[Gi2/0/01][130-E01]----[Gi2/0/01][130-E01]-> [130-E01]
    switch     cable switch side    cable patchpanel side  patchpanel  

  A white and yellow clip go to both side, so there are at least four
  clips per cable.

  There are additional indicators for special use cases.
  · additional blue for WiFi uplink
  · additional red for VoIP
  those are usually only patched and labled by the IT center

  Could you please provide a PSP element for the lables, just in case
  that other than Mrs. Leyer told, they do charge a small fee for the
  lables. (I remember that I had to buy lables in the past.)
```

Additional documentation: [German document](RWTH-Patch-Cable-Labeleling-DE.docx), [Automatic English translation](RWTH-Patch-Cable-Labeleling-EN.docx) 

## Step 1. Generate labels in a single text file

Excel sheet to generate labels from ranges: [network-patch-labels-generate-ranges.xlsx](network-patch-labels-generate-ranges.xlsx)

This Excel sheet generates Python code which can be, in turn, used to generate a labels txt file.

## Step 2. Duplicate lines

Using Notepad++ I used the following regular expression replace command to duplicate lines:
_Find `^(.*)$` and replace it with `\1\n\1`_

## Step 3. Split into two files: `white` and `yellow`

Take the room-socket duplicated labels to one file named `white-4.txt` and the switch/router port numbers to `yellow-4,txt`.

## Step 4. Split the yellow and the white lists into 48-label _layers_

I did this using the `split` command on Linux. Using the following syntax:
```
  split --lines=48 --suffix-length=1 --additional-suffix=".txt" yellow-4.txt "yellow-4-"
  split --lines=48 --suffix-length=1 --additional-suffix=".txt" white-4.txt "white-4-"
```

## Conclusion

This work was a part of my employment at the IP chair in the university. If you benefited from it, send me a social email at [orwa.diraneyya@gmail.com](mailto:orwa.diraneyya@gmail.com)
